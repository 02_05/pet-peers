<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Page</title>
<style>
.divtag{
position:relative; 
top:50px;
background-color:black;
padding:0.5%
}
.log{
position:fixed;
right:25px;
color:white;
}
.col{
color: white
}
a{
text-decoration: none;
}
table{
border-collapse:collapse;
margin-top:10%;
margin-left:auto;
margin-right:auto;
}
</style>
</head>
<body>

<div class="divtag">
<a href="#" class="col">PetShop</a>
<a href="loginPage.jsp" class="log">Login</a>
</div>
<form action="RegisterServlet" method="post">
<table>
<caption>New user? Register or Login</caption>
<tr><td>UserName</td></tr>
<tr><td><input type="text" name="username" id="name" size="50" required></td></tr>
<tr><td>Password</td></tr>
<tr><td><input type="password" name="password" size="50"  required></td></tr>
<tr><td>Confirm Password</td></tr>
<tr><td><input type="password" name="cpassword"  size="50" required></td></tr>
<tr><td><input type="submit" value="Register"></td></tr>
</table>
</form>
</body>
</html>