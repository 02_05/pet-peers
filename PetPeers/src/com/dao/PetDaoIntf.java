package com.dao;

import com.model.PetModelClass;

public interface PetDaoIntf {
	public int addPet(PetModelClass pet);
}
