package com.petshop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dbconnection.DBConnection;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		HttpSession session=request.getSession();
		response.setContentType("text/html");
		String name=request.getParameter("name");
		String password=request.getParameter("pass");
			try{
				Connection con=DBConnection.getConnection();
				PreparedStatement ps=con.prepareStatement("select * from pet_user where username=? and password=?");
				ps.setString(1,name);
				ps.setString(2,password);
				ResultSet rs=ps.executeQuery();
				if(rs.next()){
					int id=rs.getInt("id");
					session.setAttribute("id",id);
					response.sendRedirect("homePage.jsp");
				}else{
					out.println("<h2 style='text-align:center;'>Invalid credentials</h2>");
					RequestDispatcher dispatch=request.getRequestDispatcher("loginPage.jsp");
					dispatch.include(request, response);
				} 
			}catch(Exception e){
				System.out.println(e);
			}
	}

}
