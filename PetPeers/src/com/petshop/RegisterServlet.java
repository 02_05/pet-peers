package com.petshop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dbconnection.DBConnection;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String name=request.getParameter("username");
		String password=request.getParameter("password");
		String cpassword=request.getParameter("cpassword");
			try {
					Connection con=DBConnection.getConnection();
					PreparedStatement ps=con.prepareStatement("select username from pet_user where username=?");
					ps.setString(1,name);
					ResultSet rs=ps.executeQuery();
					if(rs.next()){
						out.println("<h3 style='text-align:center;'>username already exixts try with another name</h3>");
						RequestDispatcher dispatch=request.getRequestDispatcher("registrationPage.jsp");
						dispatch.include(request,response);
					}else{
						if(password.equals(cpassword)){
							PreparedStatement p=con.prepareStatement("insert into pet_user(username,password) values(?,?)");
							p.setString(1,name);
							p.setString(2,password);
							p.execute(); 
							out.println("<h2 style='text-align:center;'>Registered successfully please log in</h2>");
							RequestDispatcher dispatch=request.getRequestDispatcher("loginPage.jsp");
							dispatch.include(request,response);
						
						}else{
							out.println("<h3 style='text-align:center;'>Provide same password in two fields</h3>");
							RequestDispatcher dispatch=request.getRequestDispatcher("registrationPage.jsp");
							dispatch.include(request,response);
						}
					}


				} catch (Exception e) {
						System.out.println(e);
			}
	}

}
