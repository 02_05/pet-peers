package com.service;

import com.dao.PetDaoImpl;
import com.dao.PetDaoIntf;
import com.model.PetModelClass;

public class PetServiceImpl implements PetServiceIntf {
	PetDaoIntf dao=new PetDaoImpl();
	public int add(PetModelClass pet) {
		int row=dao.addPet(pet);
		return row;
	}

}
